#include <vector>
#include <iostream>
#include <utility>
#include <forward_list>

using namespace std;

void print_tab(const vector<int> &tab)
{
    cout << "[ ";
    for (int i = 0; i < tab.size(); i = i + 1)
    {
        cout << tab[i];
        cout << " ";
    }
    cout << "]";
}

void random_tab(vector<int> &tab)
{
    for (int i = 0; i < tab.size(); i = i + 1)
    {
        tab[i] = rand() % 100;
    }
};

void sort_tab_1(vector<int> &tab)
{
    int min;
    for (int i = 0; i < tab.size() - 1; i = i + 1)
    {
        min = i;
        for (int j = i + 1; j < tab.size(); j = j + 1)
        {
            if (tab[j] < tab[min])
            {
                min = j;
            }
        }
        if (min != i)
            swap(tab[i], tab[min]);
    };
};

void test1()
{
    vector<int> my_vector(10);
    random_tab(my_vector);
    sort_tab_1(my_vector);
    print_tab(my_vector);
}

bool less1(const int a, const int b)
{
    if (a < b)
        return true;
};

bool greater1(const int a, const int b)
{
    if (a > b)
        return true;
};

void sort_tab_2(vector<int> &tab, function<bool(const int, const int)> compare)
{
    int min;
    for (int i = 0; i < tab.size() - 1; i = i + 1)
    {
        min = i;
        for (int j = i + 1; j < tab.size(); j = j + 1)
        {
            function<bool(const int, const int)> comparateur = compare;
            if (comparateur(tab[j], tab[min]))
            {
                min = j;
            }
        }
        if (min != i)
        {
            swap(tab[i], tab[min]);
        }
    };
};

void test2()
{
    vector<int> my_vector(10);
    random_tab(my_vector);
    sort_tab_2(my_vector, [](int a, int b) { return a < b; });
    print_tab(my_vector);
    sort_tab_2(my_vector, [](int a, int b) { return a > b; });
    print_tab(my_vector);
};

forward_list<int> random_list(int n)
{
    std::forward_list<int> list;
    for (int i = 0; i < n; i = i + 1)
    {
        list.push_front(rand() % 100);
    }
    return list;
};

void print_list(const forward_list<int> &list)
{
    cout << "( ";

    for (int i : list)
    {
        cout << i;
        cout << " ";
    }
    cout << ")";
};

forward_list<int> map(const forward_list<int> &list, function<int(int)> fun)
{
    std::forward_list<int> output;
    for (int i : list)
    {
        output.push_front(fun(i));
    };
    return output;
};

forward_list<int> filter(const forward_list<int> &list, function<bool(int)> fun)
{
    std::forward_list<int> output;
    for (int i : list)
    {
        if (fun(i))
            output.push_front(i);
    };
    return output;
};

void test3()
{
    forward_list<int> list = random_list(10);
    print_list(list);
    int coeff = rand() % 5 + 1;
    cout << coeff;
    forward_list<int> second_list = map(list, [coeff](int a) { return a * coeff; });
    print_list(second_list);
    forward_list<int> third_list = filter(second_list, [](int a) { return a % 2 == 0; });
    print_list(third_list);
};

int reduce(const forward_list<int> &list, int init, function<int(int, int)> fun)
{
    int last_value = init;
    for (int i : list)
    {
        last_value = fun(last_value, i);
    }
    return last_value;
};

void test4()
{
    forward_list<int> list = random_list(10);
    print_list(list);
    int max = 0;
    int mini = reduce(list, 100, [&max](int a, int b) { if(a<b) {if(max<b)max=b; return a;} else return b; });
    cout << "Le minimum de la liste est :" << mini << " et le maximum est: " << max;
};
// Il suffit de prendre 0 et 100 pour les valeurs du deuxième argument de reduce
// car les entiers de la liste sont compris entre 0 et 99. (Nous pourrions même
// prendre 99 en deuxième paramètre si nous voulions être plus restrictif mais cela
// n'aurait pas d'interet particulier).

int main()
{
    srand(time(NULL));
    test4();
    return 0;
}

// L'ordre des 2 liste est inversé. C'est normal puisque dans une liste chainée
// le premier élément qui va être récupérer est l'élément de fin.
// A l'inverse le premier élement qui va être rajouté à notre deuxième liste output
// est l'élement de début.